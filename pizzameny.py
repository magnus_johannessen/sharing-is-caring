from tkinter import *
import tkinter.messagebox as box

window = Tk()
window.title("Order Pizza")
window.geometry("600x600")

#window.columnconfigure((0, 1, 3), weight=1)
#window.columnconfigure(2, weight=20)


window.configure(bg='powderblue')
# width=150, height=150
pizzaimage = PhotoImage(file="logo.png")
can = Canvas(window, width=150, height=150, bg='powderblue', highlightbackground = 'powderblue')
can.create_image((77,77), image=pizzaimage)

can.grid(row=0, column=1)

# Pizza types.


fPizzas = Frame(window)
fPizzas.configure()

def select():
	print("hello")

listbox = Listbox(fPizzas)
listbox.insert(1, "1) Dominos Deluxe")
listbox.insert(2, "2) Mighty Meat")
listbox.insert(3, "3) Tennessee BBQ Chicken")
listbox.insert(4, "4) Pepperoni Passion")
listbox.insert(5, "5) Grand Supreme")
listbox.insert(6, "6) Spicy Chicken")
listbox.insert(7, "7) Cheesy Bacon")
listbox.insert(8, "8) Manhattan")
listbox.insert(9, "9) Bahamas")
listbox.insert(10, "10) Primo")

listbox.pack(ipadx=15, ipady=1)

def callback(event):
    selection = event.widget.curselection()
    if selection:
        index = selection[0]
        data = event.widget.get(index)
        label.configure(text=data)
        Ingredientslist(data)
    else:
        label.configure(text="")


def Ingredientslist(data):
    global LabelIngredients
    if data == "1) Dominos Deluxe":
        LabelIngredients.configure(text="Pepperoni, skinke, ananas,\n "
                                        "sopp, 100% mozzarella & pizzasaus.")
    elif data == "2) Mighty Meat":
        LabelIngredients.configure(text="Marinert biffkjøtt, pepperoni\n"
                                        ",skinke, 100% mozzarella &\n"
                                        "pizzasaus.")
    elif data == "3) Tennessee BBQ Chicken":
        LabelIngredients.configure(text="BBQ-saus, kylling, rødløk,\n "
                                        "100% mozzarella & cheddar.")
    elif data == "4) Pepperoni Passion":
        LabelIngredients.configure(text="Pizzasaus, 100% mozzarella,\n"
                                        "dobbel pepperoni\n "
                                        "& toppet med mer mozzarella.")
    elif data == "5) Grand Supreme":
        LabelIngredients.configure(text="Marinert biffkjøtt, marinert\n"
                                        "kylling, pepperoni, bacon, \n"
                                        "rødløk, sopp, 100% mozzarella\n"
                                        "& pizzasaus.")
    elif data == "6) Spicy Chicken":
        LabelIngredients.configure(text="Marinert kylling, paprika, sopp,\n "
                                        "jalapeños, chiliflak, 100% mozzarella\n "
                                        "& pizzasaus.")
    elif data == "7) Cheesy Bacon":
        LabelIngredients.configure(text="Bacon, cheddar, 100% mozzarella\n "
                                        "& pizzasaus.")
    elif data == "8) Manhattan":
        LabelIngredients.configure(text="Pepperoni, rødløk, 100% mozzarella,\n "
                                        "pizzasaus & toppet med cheddar, sort pepper,\n "
                                        "chiliflak.")
    elif data == "9) Bahamas":
        LabelIngredients.configure(text="Skinke, bacon, ananas,\n "
                                        "100% mozzarella & pizzasaus.")
    elif data == "10) Primo":
        LabelIngredients.configure(text="Pizzasaus, 100% mozzarella, dobbel pepperoni\n "
                                        "& toppet med mer mozzarella.")
    else:
        LabelIngredients.configure(Text=" ")

listbox.bind("<<ListboxSelect>>", callback)
#fPizzas.pack(side=TOP)
fPizzas.grid(row=1, rowspan=2, column=0)
fIngre = Frame(window)
fIngre.configure(bg = 'powderblue')
label = Label(fIngre, text = "Ingredients:", fg = 'red', bg='powderblue', relief=RIDGE)
label.grid(row=1, column=2, columnspan=2, sticky=E+W)
label.columnconfigure(0, weight=3)


LabelIngredients = Label(fIngre, text = "", fg = 'red', bg='powderblue', relief=RIDGE)
LabelIngredients.config()
LabelIngredients.grid(row=2, column=2)



#fIngre.pack()
fIngre.grid(row=1, column=2, sticky=E+W)
# fIngre.grid_propagate(1)
fIngre.grid_bbox(row=1, column=2, row2=2, col2=2)
global total
total = 0


def button_click():
    if button_1 == 1:
        total +=199
        return
    elif button_2 == 2:
        total +=239
        return
    elif button_3 == 3:
        total +=239
        return
    elif button_4 == 4:
        total +=199
        return
    elif button_5 == 5:
        total +=239
        return
    elif button_6 == 6:
        total +=239
        return
    elif button_7 == 7:
        total +=159
        return
    elif button_8 == 8:
        total +=199
        return
    elif button_9 == 9:
        total +=199
        return
    elif button_10 == 10:
        total +=239
        return



button_1 = Checkbutton(window, text="1", bg="white" , command=button_click)
button_2 = Checkbutton(window, text="2", bg="white" , command=button_click)
button_3 = Checkbutton(window, text="3", bg="white" , command=button_click)
button_4 = Checkbutton(window, text="4", bg="white" , command=button_click)
button_5 = Checkbutton(window, text="5", bg="white" , command=button_click)
button_6 = Checkbutton(window, text="6", bg="white" , command=button_click)
button_7 = Checkbutton(window, text="7", bg="white" , command=button_click)
button_8 = Checkbutton(window, text="8", bg="white" , command=button_click)
button_9 = Checkbutton(window, text="9", bg="white" , command=button_click)
button_10 = Checkbutton(window, text="10", bg="white" , command=button_click)

button_1.grid(row=5, column=0)
button_2.grid(row=5, column=1)
button_3.grid(row=5, column=2)
button_4.grid(row=6, column=0)
button_5.grid(row=6, column=1)
button_6.grid(row=6, column=2)
button_7.grid(row=7, column=0)
button_8.grid(row=7, column=1)
button_9.grid(row=7, column=2)
button_10.grid(row=8, column=0)

base = StringVar()

fBase = Frame(window)

fIngre.configure(bg = 'powderblue')

radioThick = Radiobutton(fBase, text = "Thick", variable = base, value = "thick")
radioThin = Radiobutton(fBase, text = "Thin", variable = base, value = "thin")

radioThick.select()

radioThick.grid(row=9, column=1)
radioThin.grid(row=10, column=1)

fBase.grid(row=10, column=1)

button_11 = Button(window, text="Print Price" , command=print(total))
button_11.grid(row=12, column=1)
window.mainloop()

