from tkinter import *

nissa = Tk()
nissa.title("Calculator")
nissa.geometry("325x400")


def button_click(number):
    current = e.get()
    e.delete(0, END)
    e.insert(0, str(current) + str(number))


def button_add():
    first_number = e.get()
    global f_num
    global math
    math = "addition"
    f_num = float(first_number)
    e.delete(0, END)


def button_clear():
    e.delete(0, END)


def button_subtract():
    first_number = e.get()
    global f_num
    global math
    math = "subtraction"
    f_num = float(first_number)
    e.delete(0, END)


def button_divide():
    first_number = e.get()
    global f_num
    global math
    math = "division"
    f_num = float(first_number)
    e.delete(0, END)


def button_multiply():
    first_number = e.get()
    global f_num
    global math
    math = "multiplication"
    f_num = float(first_number)
    e.delete(0, END)


def button_equal():
    second_number = e.get()
    e.delete(0, END)
    if math =="addition":
            e.insert(0, f_num + float(second_number))
    elif math == "subtraction":
        e.insert(0, f_num - float(second_number))
    elif math == "division":
            e.insert(0, f_num / float(second_number))
    elif math == "multiplication":
                e.insert(0, f_num * float(second_number))


def show_point():
    if e.get()==".":
        pass
    else:
        e.insert(END,".")


e = Entry(nissa, width=24, borderwidth=5, fg="black", bg="white", font='Times 18 bold')
e.grid(row=0, column=0, columnspan=4, padx=10, pady=10, ipady=7)

# Define buttons

button_comma = Button(nissa, text=".", padx=29.499, pady=20, bg="white" , command=show_point)
button_0 = Button(nissa, text="0", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(0))
button_1 = Button(nissa, text="1", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(1))
button_2 = Button(nissa, text="2", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(2))
button_3 = Button(nissa, text="3", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(3))
button_4 = Button(nissa, text="4", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(4))
button_5 = Button(nissa, text="5", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(5))
button_6 = Button(nissa, text="6", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(6))
button_7 = Button(nissa, text="7", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(7))
button_8 = Button(nissa, text="8", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(8))
button_9 = Button(nissa, text="9", font='Times 18 bold', padx=20, pady=10, bg="white" , command=lambda: button_click(9))
button_clear = Button(nissa, text="CE", font='Times 18 bold', padx=49, pady=10, bg="lightgrey" , command=button_clear)
button_equal = Button(nissa, text="=", font='Times 18 bold', padx=20, pady=42, bg="lightgrey" , command=button_equal)
button_add = Button(nissa, text="+", font='Times 18 bold', padx=19, pady=10, bg="lightgrey" , command=button_add)
button_multiply = Button(nissa, text="X", font='Times 18 bold', padx=19, pady=10, bg="lightgrey" , command=button_multiply)
button_subtract = Button(nissa, text="-", font='Times 18 bold', padx=23, pady=10, bg="lightgrey" , command=button_subtract)
button_divide = Button(nissa, text="÷", font='Times 18 bold', padx=20.499, pady=10, bg="lightgrey" , command=button_divide)

# put buttons on screen

button_clear.grid(row=1, column=0, columnspan=2)
button_divide.grid(row=1, column=3)

button_7.grid(row=2, column=0)
button_8.grid(row=2, column=1)
button_9.grid(row=2, column=2)
button_multiply.grid(row=2, column=3)

button_4.grid(row=3, column=0)
button_5.grid(row=3, column=1)
button_6.grid(row=3, column=2)
button_subtract.grid(row=3, column=3)


button_1.grid(row=4, column=0)
button_2.grid(row=4, column=1)
button_3.grid(row=4, column=2)


button_0.grid(row=5, column=0)
button_comma.grid(row=5, column=1)
button_equal.grid(row=4, column=3, rowspan=2)
button_add.grid(row=5, column=2)

nissa.mainloop()
