animal = 'Badgers'
# This says that the animal is a badger
amount = 3
# This says the amount is 3 of them
print ('There once was', amount, animal, '(s)', sep='_', end="~~\n")
# This prints out the commands that was defined earlier

# Alternate way to do it.
A = "There once was"
print (A, amount, animal, '(s)', end="~~\n")
# Modified to use A in print instead of text predefined